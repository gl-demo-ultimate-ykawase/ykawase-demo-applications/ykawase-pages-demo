+++
title = 'Gitbasicshandsonlab6'
date = 2024-07-03T12:18:33+09:00
draft = false
+++

---
title: "GitLabとGit Essentials - ハンズオンラボ: 静的アプリケーションセキュリティテスト（SAST）"
description: "このハンズオンガイドでは、コード内のセキュリティの脆弱性を追跡するためのSASTジョブの設定方法について説明します。"
---

> 完了までの見積時間: 30分

## 目的

このラボでは、CI/CDパイプラインのオプション機能であるSASTを使用して、コード内のセキュリティの脆弱性を特定します。GitLabの脆弱性レポートでは、各パイプライン実行で見つかった古いまたは新しい脆弱性が表示されます。詳細については、[ドキュメント](https://docs.gitlab.com/ee/user/application_security/sast/)を参照してください。

## タスクA. `CI Test`プロジェクトでSASTを有効にする

1. **CI Test**プロジェクトに移動します。

1. `.gitlab-ci.yml`ファイルをクリックし、**編集 > 単一ファイルを編集**をクリックします。

1. 次の行を`gitlab-ci.yml`ファイルの最後にコピーします:

    ```yaml
    include:
      - template: Jobs/SAST.gitlab-ci.yml
    ```

    > `SAST`スキャンを`.gitlab-ci.yml`ファイルに統合する方法について詳しくは、[ドキュメント](https://docs.gitlab.com/ee/user/application_security/sast/#configure-sast-in-your-cicd-yaml)を参照してください。

1. 現在の`.gitlab-ci.yml`ファイルは次のようになります:

    ```yaml
    stages:
      - build
      - test

    build1:
      stage: build
      script:
        - echo "Do your build here"

    test1:
      stage: test
      script:
        - echo "Do a test here"
        - echo "For example run a test suite"

    include:
      - template: Jobs/SAST.gitlab-ci.yml
    ```

    > **include**は、CI/CD構成で外部のYAMLファイルを含めることができます。1つの長い`.gitlab-ci.yml`ファイルを複数のファイルに分割して可読性を向上させたり、同じ構成を複数の場所で重複させることを減らすことができます。`include`キーワードについて詳しくは、[ドキュメント](https://docs.gitlab.com/ee/ci/yaml/#include)を参照してください。

1. 適切な**コミットメッセージ**を入力します。

1. **ターゲットブランチ**を`main`に設定します。

1. **変更をコミット**ボタンをクリックします。

## タスクB. `run.py`を追加し、SASTスキャン結果を確認する

このタスクでは、既知の脆弱性を持つファイルを追加し、SASTがそれを検出するかどうかを確認します。

1. パンくずリストセクションでプロジェクト名をクリックして、**プロジェクトの概要**ページに戻ります。

1. プロジェクトランディングページの上部で、ブランチドロップダウンの右側にある、**(+) > このディレクトリ > 新しいファイル**をクリックします。

1. **ファイル名**フィールドに`run.py`と入力してください。

1. 以下の内容をファイルにコピーしてください：

    ```python
    import subprocess

    in = input("Enter your server ip: ")
    subprocess.run(["ping", in])

    print("Attempting to connect to the server")
    print("Application authentication was successful")
    ```

1. 適切な**コミットメッセージ**を追加してください。

1. **ターゲットブランチ**を`main`に設定してください。

1. **変更をコミット**ボタンをクリックしてください。

1. 左側のナビゲーションパネルで**ビルド> パイプライン**をクリックしてください。

1. パイプラインのテーブルの行の上部で、**running**（まだ実行中の場合）または**passed**（パイプラインが完了した場合）ステータスラベルをクリックしてください。

    > SASTスキャンには少し時間がかかる場合がありますので、待っている間にコーヒーを飲んでください。

1. パイプラインが完了したら、左側のナビゲーションパネルで**Secure > 脆弱性レポート**をクリックしてください。

1. いずれかの脆弱性をクリックし、`run.py`でSASTスキャンで検出された潜在的なセキュリティ問題について読んでください。

1. 問題を修正するためにコードを編集しても構いません（例：`subprocess.run`コマンドを削除する）、変更をコミットしてください。脆弱性レポートは問題がまだ存在するとしていますか？

## ラボガイド完了

このラボ演習が完了しました。他の[このコースのラボガイド](/handbook/customer-success/professional-services-engineering/education-services/gitbasicshandson)をご覧いただけます。

## 提案はありますか？

ラボに変更を提案したい場合は、マージリクエストを介して提出してください。

